var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var BillSchema = new Schema({
    userid: String,
    date: {type: Date, default: Date.now},
    status: {type: String, default: "Đang gửi"},
    booklist: String,
    total: Number,
    email: String,
    fullname: String,
    address: String,
    zipcode: String,
    country: String,
    region: String,
    phone: String
});

module.exports = mongoose.model('Bill', BillSchema);
