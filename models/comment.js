var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var CommentSchema = new Schema({
    name: String,
    bookid: String,
    content: String,
    date: Number
});

module.exports = mongoose.model('Comment', CommentSchema);
