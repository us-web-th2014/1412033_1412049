var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var CartSchema = new Schema({
    userid: String,
    session: String,
    booklist: String,
    total: Number
});

module.exports = mongoose.model('Cart', CartSchema);
