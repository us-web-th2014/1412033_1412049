var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var Schema = mongoose.Schema;
var UserSchema = new Schema({
    username: String,
    isactived: {type: Boolean, default: false},
    fullname: String,
    address: String,
    zipcode: String,
    country: String,
    region: String,
    phone: String,
    avatar: {type: String, default: "noavatar"},
    role: {type: String, default: "ROLE_USER"}
});

UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', UserSchema);
