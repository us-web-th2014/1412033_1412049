var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var BookSchema = new Schema({
    name: String,
    category: String,
    originalcost: Number,
    cost: Number,
    year: Number,
    author: String,
    avatar: {type: String, default: "noavatar"},
    instock: Number,
    totalcost: {type: Number, default: 0},
    description: {type: String, default: ""},
    nview: {type: Number, default: 0}
});

module.exports = mongoose.model('Book', BookSchema);
