var express = require('express');
var router = express.Router();
var auth = require('../controllers/authController');
var userController = require('../controllers/dashboard/userController');
var bookController = require('../controllers/dashboard/bookController');
var billController = require('../controllers/dashboard/billController');
var commentController = require('../controllers/dashboard/commentController');
var indexController = require('../controllers/dashboard/indexController');

router.get('/', auth.isAdmin, indexController.index);

router.get('/login', function (req, res, next) {
    res.render('dashboard/login');
});
router.post('/login', auth.AdminLogin);
router.get('/logout', auth.AdminLogout);

/**
 * USER
 * */
router.get('/user', auth.isAdmin, userController.index);
router.get('/user/new', auth.isAdmin, userController.new);
router.get('/user/edit', auth.isAdmin, userController.info);
router.get('/user/del', auth.isAdmin, userController.remove);
router.post('/user/save', auth.isAdmin, userController.insert);

/**
 * BOOK
 * */
router.get('/book', auth.isAdmin, bookController.index);
router.get('/book/new', auth.isAdmin, bookController.new);
router.get('/book/edit', auth.isAdmin, bookController.info);
router.get('/book/del', auth.isAdmin, bookController.remove);
router.post('/book/save', auth.isAdmin, bookController.insert);

/**
 * BILL
 * */
router.get('/bill', auth.isAdmin, billController.index);
router.get('/bill/del', auth.isAdmin, billController.remove);
router.get('/bill/done', auth.isAdmin, billController.done);

/**
 * COMMENT
 * */
router.get('/comment', auth.isAdmin, commentController.index);
router.get('/comment/del', auth.isAdmin, commentController.remove);

module.exports = router;
