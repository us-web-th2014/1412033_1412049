var express = require('express');
var homeController = require('../controllers/homeController');
var authController = require('../controllers/authController');
var categoryController = require('../controllers/categoryController');
var bookController = require('../controllers/bookController');
var cartController = require('../controllers/cartController');
var billController = require('../controllers/billController');
var userController = require('../controllers/userController');
var router = express.Router();


/**
 * HOME PAGE
 */
router.get('/', homeController.index);
router.get('/search', homeController.search);
router.post('/searchs', homeController.searchs);
router.post('/searcha', homeController.searcha);

/**
 * USER LOGIN
 */
router.get('/login', authController.UserLogin);
router.get('/logout', authController.UserLogout);
router.post('/login', authController.doUserLogin);
router.post('/register', authController.doUserRegister);

router.get('/user', authController.isUser, userController.index);
router.post('/user', authController.isUser, userController.edit);
router.get('/userpass',authController.isUser, userController.pass);
router.post('/userpass',authController.isUser, userController.dopass);

router.get('/lostpass', userController.lostpass);
router.post('/lostpass', userController.dolostpass);
router.get('/resetpass', userController.reset);

router.get('/bill', authController.isUser, userController.bill);

/**
 * CONTACT
 */
router.get('/contact', homeController.contact);

/**
 * CATEGORY
 */
router.get('/category', categoryController.index);

/**
 *  BOOK
 */
router.get('/book', bookController.index);
router.post('/book', bookController.doComment);
router.post('/bookcmt', bookController.doMore);

/**
 *  CART
 */
router.post('/addbook', cartController.addbook);
router.post('/cartcq', cartController.changeQuantity);
router.post('/cartdel', cartController.delBook);
router.get('/cart', cartController.index);

/**
 *  CHECKOUT (BILL)
 */
router.get('/checkout', authController.isUser, billController.index);
router.post('/checkout', authController.isUser, billController.doCheckout);

module.exports = router;
