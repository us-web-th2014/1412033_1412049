var Cart = require('../models/cart');
var Book = require('../models/book');
var cartController = {};

cartController.index = function (req, res, next) {
    var userid = (req.user) ? req.user._id : "0";
    Cart.findOne({session: req.session.id, userid: userid}, function (err, cart) {
        if (err) return res.send("ERROR");
        if (cart) {
            var lstb = JSON.parse(cart.booklist);
            res.render('cart', {title: "Giỏ hàng", ct: -1, user: req.user, cart: cart, lstb: lstb, ghrong: false});
        } else {
            return res.render('cart', {title: "Giỏ hàng", ct: -1, user: req.user, ghrong: true});
        }
    });
};

cartController.addbook = function (req, res, next) {
    var userid = (req.user) ? req.user._id : "0";
    var gcart;
    Cart.findOne({session: req.session.id, userid: userid}).exec()
        .then(function (cart) {
            gcart = cart;
            return Book.findById(req.body.id).exec();
        })
        .then(function (book) {
            if (book.instock < 0) return res.send("ERROR");
            if (gcart) {
                var lstb = JSON.parse(gcart.booklist);
                var t = true;
                lstb.forEach(function (p) {
                    if (p.id == book._id) {
                        t = false;
                    }
                });
                if (t) {
                    lstb[lstb.length] = {
                        'id': book._id,
                        'name': book.name,
                        'cost': book.cost,
                        'quantity': 1,
                        'totalcost': book.cost
                    };
                    gcart.booklist = JSON.stringify(lstb);
                    gcart.total += book.cost;
                }
                gcart.save();
            } else {
                var lstb = [];
                lstb[0] = {
                    'id': book._id,
                    'name': book.name,
                    'cost': book.cost,
                    'quantity': 1,
                    'totalcost': book.cost
                };
                var cart = new Cart({
                    'userid': userid,
                    'session': req.session.id,
                    'booklist': JSON.stringify(lstb),
                    'total': book.cost
                });
                cart.save();
            }
            res.send("OK");
        })
        .catch(function (err) {
            res.send("ERROR");
        });
};

cartController.changeQuantity = function (req, res, next) {
    var userid = (req.user) ? req.user._id : "0";
    Cart.findOne({session: req.session.id, userid: userid}, function (err, cart) {
        if (err) return res.send("ERROR");
        if (cart) {
            var lstb = JSON.parse(cart.booklist);
            lstb.forEach(function (p) {
                if (p.id == req.body.id) {
                    p.quantity = req.body.quantity;
                    cart.total -= p.totalcost;
                    p.totalcost = p.cost * p.quantity;
                    cart.total += p.totalcost;
                }
            });
            cart.booklist = JSON.stringify(lstb);
            cart.save();
            var ress = [];
            ress[0] = lstb;
            ress[1] = cart.total;
            res.json(ress);
        } else {
            res.send("ERROR");
        }
    });
};

cartController.delBook = function (req, res, next) {
    var userid = (req.user) ? req.user._id : "0";
    Cart.findOne({session: req.session.id, userid: userid}, function (err, cart) {
        if (err) return res.send("ERROR");
        if (cart) {
            var lstb = JSON.parse(cart.booklist);
            for (var i = 0; i < lstb.length; i++) {
                if (lstb[i].id == req.body.id) {
                    cart.total -= lstb[i].totalcost;
                    lstb.splice(i, 1);
                    break;
                }
            }
            if (lstb.length == 0) {
                cart.remove();
                return res.send("NULL");
            }
            cart.booklist = JSON.stringify(lstb);
            cart.save();
            var ress = [];
            ress[0] = lstb;
            ress[1] = cart.total;
            res.json(ress);
        } else {
            res.send("ERROR");
        }
    });
};

module.exports = cartController;
