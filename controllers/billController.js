var Cart = require('../models/cart');
var Bill = require('../models/bill');
var Book = require('../models/book');

var billController ={};

billController.index = function (req, res, next) {
    var userid = req.user._id;
    Cart.findOne({session: req.session.id, userid: userid}, function (err, cart) {
        if (err) return res.send("ERROR");
        if (cart) {
            var lstb = JSON.parse(cart.booklist);
            res.render('checkout', {title: "Giỏ hàng", ct: -1, user: req.user, cart: cart, lstb: lstb, ghrong: false});
        } else {
            res.render('checkout', {title: "Thanh toán", ct: -1, user: req.user, ghrong: true})
        }
    });
};

billController.doCheckout = function (req, res, next) {
    Cart.remove({_id: req.body.cartid}, function (err) {
        if(err) console.log(err);
    });

    var lstb = JSON.parse(req.body.booklist);
    lstb.forEach(function (p) {
        Book.findById(p.id, function(err, book){
            book.instock -= p.quantity;
            book.totalcost += p.totalcost;
            book.save(function (err) {
                if(err) console.log(err);
            })
        });
    });

    var bill = new Bill({
        userid: req.body.userid,
        booklist: req.body.booklist,
        total: req.body.total,
        email: req.body.email,
        fullname: req.body.fullname,
        address: req.body.address,
        zipcode: req.body.zipcode,
        country: req.body.country,
        region: req.body.region,
        phone: req.body.phone
    });
    bill.save(function (err) {
        if (err) return res.send("ERROR");
        res.redirect('/bill');
    })
};

module.exports = billController;
