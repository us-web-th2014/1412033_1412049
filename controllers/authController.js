var mongoose = require('mongoose');
var passport = require('passport');
var User = require('../models/user');
var Cart = require('../models/cart');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var authController = {};

/**
 *  ADMIN SECTION
 * */
authController.isAdmin = function (req, res, next) {
    if (!req.user || (req.user && req.user.role != "ROLE_ADMIN")) {
        res.redirect('/dashboard/login');
    } else {
        next();
    }
};
authController.AdminLogin = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user || (user.role != "ROLE_ADMIN")) {
            return res.redirect('/dashboard/login');
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/dashboard');
        });
    })(req, res, next);
};
authController.AdminLogout = function (req, res) {
    req.logOut();
    res.redirect('/dashboard/login');
};

/**
 *  USER SECTION
 * */
authController.isUserInactived = function (req, res, next) {
    if (req.user && !req.user.isactived) {
        return res.redirect('/active');
    } else {
        next();
    }
};
authController.isUser = function (req, res, next) {
    if (!req.user) {
        res.redirect('/login');
    } else {
        next();
    }
};
authController.UserLogin = function (req, res, next) {
    if (req.user) {
        return res.redirect('/');
    }
    res.render('login', {title: "Đăng nhập", ct: -1, user: req.user})
};
authController.UserLogout = function (req, res) {
    req.logOut();
    req.session.destroy();
    res.redirect('/');
};
authController.doUserLogin = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/login');
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }

            Cart.findOne({userid: user._id}, function (err, cart) {
                if (err) return;
                if (cart) {
                    cart.session = req.session.id;
                    cart.save(function (err) {
                        Cart.find({session: req.session.id, userid: "0"}, function (err, carts) {
                            carts.forEach(function (p) { p.remove(); });
                        });
                    });
                } else {
                    Cart.findOne({session: req.session.id}, function (err, cart) {
                        if (cart) {
                            cart.userid = user._id;
                            cart.save();
                        }
                    });
                }
            });

            return res.redirect('/');
        });
    })(req, res, next);
};
authController.doUserRegister = function (req, res, next) {
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../public/uploads/avatar');
    form.parse(req, function (err, fields, files) {
        if (err) {
            res.redirect('/login');
            return;
        }
        var stravatar = files.avatar.path.split("\\");
        stravatar = stravatar[stravatar.length - 1];
        if (files.avatar.size == 0) {
            fs.unlinkSync(files.avatar.path);
            stravatar = "noavatar";
        }
        var user = new User({
            username: fields.username,
            fullname: fields.fullname,
            address: fields.address,
            zipcode: fields.zipcode,
            country: fields.country,
            region: fields.region,
            phone: fields.phone,
            avatar: stravatar
        });
        User.register(user, fields.password, function (err, user) {
            if (err)
                console.log(err);
            req.body.username = fields.username;
            req.body.password = fields.password;
            passport.authenticate('local')(req, res, function () {
                res.redirect('/active');
            });
        });
    });
};
authController.UserActive = function (req, res, next) {
    if (req.user.isactived) {
        return res.redirect('/');
    }

    const nodemailer = require('nodemailer');

    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
            user: 'bshop.ml@gmail.com',
            pass: '123456Qa'
        }
    });

    var mailOptions = {
        from: '"Admin Bshop" <bshop.ml@gmail.com>',
        to: req.user.username,
        subject: "Kích họa tài khoản",
        text: '',
        html: 'Xin mời bạn nhấn vào đường link bên dưới để kích hoạt tài khoản<br><a href="http://localhost:3000/activelink?ss='
        + req.user._id + '&s=' + req.user.username + '">Kích hoạt tại đây</a>'
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            return console.log(err);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
    res.render('active', {title: "Kích hoạt tài khoản", ct: -1, user: req.user, laactive: true});
};
authController.UserCheckActive = function (req, res, next) {
    if (req.user.isactived) {
        return res.redirect('/');
    }
    if (req.user._id == req.query.ss && req.user.username == req.query.s) {
        User.findById(req.user._id, function (err, user) {
            if (err) {
                console.log(err);
                return res.redirect('/');
            }
            user.isactived = true;
            user.save(function (err) {
                if (err) console.log(err);
                res.redirect('/');
            })
        });
    } else {
        res.redirect('/active');
    }
};

/**
 *  EXPORT
 * */
module.exports = authController;
