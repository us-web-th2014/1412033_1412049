var User = require('../models/user');
var Cart = require('../models/cart');
var Bill = require('../models/bill');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');

var userController = {};

userController.index = function (req, res, next) {
    res.render('user', {title: "Tài khoản", ct: -1, user: req.user});
};

userController.edit = function (req, res, next) {
    var user = req.user;
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../public/uploads/avatar');
    form.parse(req, function (err, fields, files) {
        if(err) {
            res.redirect('/user');
            return;
        }

        var stravatar = files.avatar.path.replace(/^.*[\\\/]/, '');

        if (files.avatar.size == 0) {
            fs.unlinkSync(files.avatar.path);
            stravatar = "noavatar";
        }
        user.fullname= fields.fullname;
        user.address= fields.address;
        user.phone= fields.phone;
        user.region= fields.region;
        user.country= fields.country;
        user.zipcode= fields.zipcode;
        if (stravatar != "noavatar") {
            user.avatar= stravatar;
        }
        user.save(function (err) {
            if (err)
                console.log(err);
            res.redirect('/user');
        });

    });
};

userController.pass = function (req, res, next) {
    res.render('userpass', {title: "Tài khoản", ct: -1, user: req.user, loi: ""});
};
userController.dopass = function (req, res, next) {
    var oldpass = req.body.oldpassword;
    var newpass = req.body.newpassword;
    req.user.authenticate(oldpass, function (err, user, passerr) {
        if (err) return res.render('userpass', {title: "Tài khoản", ct: -1, user: req.user, loi: err});
        if (!user) return res.render('userpass', {title: "Tài khoản", ct: -1, user: req.user, loi: "Mật khẩu cũ không chính xác"});
        user.setPassword(newpass, function (err, user, passerr) {
            if (err) return res.render('userpass', {title: "Tài khoản", ct: -1, user: req.user, loi: err});
            if (!user) return res.render('userpass', {title: "Tài khoản", ct: -1, user: req.user, loi: "Lỗi"});
            user.save(function (err) {
                console.log(err);
                res.redirect('/user');
            })
        })
    });
};

userController.lostpass = function (req, res, next) {
    res.render('active', {title: "Kích hoạt tài khoản", ct: -1, user: req.user, laactive: false});
};
userController.dolostpass = function (req, res, next) {

    User.findOne({username: req.body.email}, function (err, user) {
        if(err || !user) return res.send("ERROR");
        const nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            auth: {
                user: 'bshop.ml@gmail.com',
                pass: '123456Qa'
            }
        });

        var mailOptions = {
            from: '"Admin Bshop" <bshop.ml@gmail.com>',
            to: user.username,
            subject: "Reset mật khẩu",
            text: '',
            html: 'Xin mời bạn nhấn vào đường link bên dưới để đổi mật khẩu của bạn thành "123456" <br><a href="http://localhost:3000/resetpass?ss='
            + user._id + '&s=' + user.username + '">Kích hoạt tại đây</a>'
        };

        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                return console.log(err);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });

        res.redirect('/login');
    });
};

userController.reset = function (req, res, next) {
    var id = req.query.ss;
    var username = req.query.s;
    User.findOne({_id: id, username: username}, function (err, user) {
        if (err || !user) return res.send("ERROR");;
        user.setPassword("123456", function (err, user, passerr) {
            if (err || !user) return res.send("ERROR");
            user.save(function (err) {
                console.log(err);
                res.redirect('/login');
            })
        });
    });
}

userController.bill = function (req, res, next) {
    if(!req.query.id) {
        Bill.find({userid: req.user._id}, function (err, bills) {
            if (err) return res.send("ERROR");
            res.render('userbill', {title:'Hóa đơn', ct: -1, user: req.user, cthd: false, bills: bills});
        })
    } else {
        Bill.findById(req.query.id, function (err, bill) {
            if (err || !bill) return res.send("ERROR");
            var lstb = JSON.parse(bill.booklist);
            res.render('userbill', {title:'Hóa đơn', ct: -1, user: req.user, cthd: true, bill: bill, lstb: lstb});
        });
    }
};

module.exports = userController;

