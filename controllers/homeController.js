var Book = require('../models/book');
var homeControlller = {};

homeControlller.index = function (req, res, next) {
    var lstbook1;
    Book.find({}).sort({'totalcost': -1}).limit(6).exec()
        .then(function (books) {
            lstbook1 = books;
            return Book.find({}).sort({'_id': -1}).limit(6).exec()
                .then(function (books) {
                    return books;
                })
        })
        .then(function (books) {
            res.render('index', {title: "Trang chủ", ct: -1, user: req.user, books1: lstbook1, books2: books});
        })
        .catch(function (err) {
            console.log(err);
        });

};

homeControlller.contact = function (req, res, next) {

    res.render('contact', {title: "Liên hệ", ct: -1, user: req.user});
};

homeControlller.search = function (req, res, next) {
    res.render('search', {title: "Tìm kiếm", ct: -1, user: req.user, books: []})
};

homeControlller.searchs = function (req, res, next) {
    var bn = req.body.name;
    Book.find({'name' : new RegExp(bn, 'i')}).limit(24).exec(function (err, books) {
        if(err) return res.send("ERROR");
        res.render('search', {title: "Tìm kiếm", ct: -1, user: req.user, books: books});
    });
};

homeControlller.searcha = function (req, res, next) {
    var bn = req.body.name;
    var category = req.body.category;
    var au = req.body.author;
    var fi = req.body.from <= 1990 ? 1990 : req.body.from - 1;
    var ti = req.body.to <= 1990 ? 2020 : req.body.to + 1;
    if (fi >= ti) {
        ti += 1;
    }
    Book.find({'name' : new RegExp(bn, 'i'), category: category, author: new RegExp(au, 'i'), year: {$gt: fi, $lt: ti}}).limit(24).exec(function (err, books) {
        if(err) return res.send("ERROR");
        res.render('search', {title: "Tìm kiếm", ct: -1, user: req.user, books: books});
    });
};

module.exports = homeControlller;
