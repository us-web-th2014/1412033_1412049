var Book = require('../models/book');
var categoryControlller = {};

const lstcategory = ["Chính Trị",
    "Đoàn - Đội - Hội",
    "Kiến Thức - Khoa Học",
    "Kinh Tế",
    "Kỹ Năng",
    "Lịch Sử",
    "Mầm Non",
    "Nghệ Thuật - Giải Trí",
    "Nghệ Thuật Sống",
    "Ngôn Ngữ",
    "Sinh Ngữ",
    "Tâm Lý - Giáo Dục",
    "Tham Khảo Trong Nhà Trường",
    "Truyện Tranh",
    "Văn Hóa - Khảo Cứu",
    "Văn Học",
    "Y Học - Sức Khỏe"];

categoryControlller.index = function (req, res, next) {
    var p = req.query.p ? req.query.p - 1 : 0;
    var poffset = 0;
    var n = 0;
    Book.count({'category': lstcategory[req.query.t]}).exec()
        .then(function (count) {
            poffset = count > 12 ? p * 12 : 0;
            n = count / 12 + (count % 12 > 0 ? 1 : 0);
            return Book.find({'category': lstcategory[req.query.t]}).skip(poffset).limit(12).exec()
                .then(function (books) {
                    return books;
                });
        })
        .then(function (books) {
            res.render('category', {title: lstcategory[req.query.t], ct: req.query.t, user: req.user, books: books, p: p+1, n: n});
        })
        .catch(function (err) {
            console.log(err);
            res.send("ERROR");
        });
};

module.exports = categoryControlller;