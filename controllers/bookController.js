var Book = require('../models/book');
var Comment = require('../models/comment');
var bookController ={};

bookController.index = function (req, res, next) {
    var b, bss, cmts;
    Book.findById(req.query.id).exec()
        .then(function (book) {
            b = book;
            return Book.find({'_id': {$ne: b._id}, 'category': b.category}).sort({'totalcost': -1}).limit(6).exec();
        })
        .then(function (books) {
            bs = books;
            return Comment.find({'bookid': b._id}).sort({'date': -1}).exec();
        })
        .then(function (comments) {
            b.nview++;
            b.save(function (err) {
                if (err) {
                    console.log(err);
                }
            });
            res.render('book', {title: b.name, ct: -1, user: req.user, book: b, books: bs, comments: comments});
        })
        .catch(function (err) {
            if (err) {
                console.log(err);
                res.redirect('/');
            }
        })

};

bookController.doComment = function (req, res, next) {
    Book.findById(req.query.id, function (err, book) {
        if (err) {
            console.log(err);
            return res.send("ERROR");
        }
        var sname = req.body.name + ' (' + req.body.email +')';
        var c = req.body.scmt;
        var cmt = new Comment({
            name: sname,
            bookid: book._id,
            content: c,
            date : new Date().getTime()
        });
        cmt.save(function (err) {
            console.log(err);
            res.send("OK");
        });
    });
};

bookController.doMore = function (req, res, next) {
    var datei = req.body.date;
    var bid = req.query.id;
    Comment.find({'date' : {$gt: datei}, 'bookid': bid}, function (err, comments) {
        if (err) {
            console.log(err);
            return res.send("ERROR");
        }
        if (comments.length == 0) {
            return res.send("ERROR");
        }
        var ress = {
            'name': comments[0].name,
            'date': comments[0].date,
            'content': comments[0].content
        };
        //res.send('['+JSON.stringify(ress)+']');
        res.json(ress);
    });
}

module.exports = bookController;
