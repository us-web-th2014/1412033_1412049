var Book = require('../../models/book');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var bookController = {};

bookController.index = function (req, res, next) {
    Book.find({}, function (err, books) {
        if (err) {
            console.log(err);
            res.render('dashboard/book', {title: 'Book Manager', username: req.user.username, books: []});
        }
        else
            res.render('dashboard/book', {title: "Book manager", username: req.user.username, books: books});
    });
};

bookController.info = function (req, res, next) {
    Book.findById(req.query.id, function (err, book) {
        if (err) {
            console.log(err);
            return res.redirect('/dashboard/book');
        }
        res.render('dashboard/bookinfo', {title: 'Edit Book', username: req.user.username, book: book});
    });
};

bookController.insert = function (req, res, next) {
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../../public/uploads/imgs');
    form.parse(req, function (err, fields, files) {
        if (err) {
            res.redirect('/dashboard/book');
            return;
        }

        var stravatar = files.avatar.path.replace(/^.*[\\\/]/, '');

        if (files.avatar.size == 0) {
            fs.unlinkSync(files.avatar.path);
            stravatar = "noavatar";
        }
        if (fields.id == "auto") {
            var book = new Book({
                name: fields.name,
                category: fields.category,
                originalcost: fields.originalcost,
                cost: fields.cost,
                year: fields.year,
                author: fields.author,
                avatar: stravatar,
                instock: fields.instock,
                description: fields.description
            });
            book.save(function (err) {
                if (err) console.log(err);
                return res.redirect('/dashboard/book');
            })
        } else {
            Book.findById(fields.id, function (err, book) {
                if (err) {
                    res.redirect('/dashboard/user');
                    return;
                }
                book.name = fields.name;
                book.category = fields.category;
                book.originalcost = fields.originalcost;
                book.cost = fields.cost;
                book.year = fields.year;
                book.author = fields.author;
                book.description = fields.description;
                if (stravatar != "noavatar") {
                    book.avatar = stravatar;
                }
                book.instock = fields.instock;
                book.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                    res.redirect('/dashboard/book');
                })
            });
        }
    });
};

bookController.remove = function (req, res, next) {
    Book.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send('OK');
        }
    });
};

bookController.new = function (req, res, next) {
    res.render('dashboard/bookinfo', {
        title: 'New Book', username: req.user.username,
        book: {
            _id: "auto",
            name: "",
            originalcost: 0,
            cost: 0,
            year: 1990,
            author: "",
            instock: 0,
        }
    });
};

module.exports = bookController;
