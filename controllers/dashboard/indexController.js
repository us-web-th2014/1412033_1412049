var Book = require('../../models/book');
var indexControlller = {};

indexControlller.index = function (req, res, next) {

    Book.aggregate([
        {
            $group : {
                _id : "$category",
                total: { $sum: "$totalcost" }
            }
        }
    ], function (err, results) {
        if (err) console.log(err);
        var lb = []; var i = 0;
        var se = [];
        results.forEach(function (p) {
            lb[i] = p._id;
            se[i] = p.total;
            i++;
        });
        Book.find({}).sort({totalcost: -1}).limit(10).exec(function (err, books) {
            if (err) console.log(err);
            res.render('dashboard/index', {title: 'Dashboard', username: req.user.username, books: books, lb: lb, se: se});
        });
    });


};

module.exports = indexControlller;
