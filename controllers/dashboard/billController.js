var Bill = require('../../models/bill');
var billController = {};

billController.index = function (req, res, next) {
    Bill.find({}, function (err, bills) {
        if (err) {
            return res.send("ERROR");
        }
        else
            res.render('dashboard/bill', {title: "Book manager", username: req.user.username, bills: bills});
    });
};

billController.done = function (req, res, next) {
    Bill.findById(req.query.id, function (err, bill) {
        if (err || !bill) return res.send("ERROR");
        bill.status = "Đã nhận";
        bill.save(function (err) {
            if (err) return res.send("ERROR");
            res.send("OK");
        });
    });
};

billController.remove = function (req, res, next) {
    Bill.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send('OK');
        }
    });
};

module.exports = billController;
