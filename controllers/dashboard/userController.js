var User = require('../../models/user');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var userController = {};

userController.index = function (req, res, next) {

    User.find({}, function (err, users) {
        if (err) {
            console.log(err);
            res.render('dashboard/user', {title: 'User Manager', username: req.user.username, users: []});
        }
        else
            res.render('dashboard/user', {title: "User manager", username: req.user.username, users: users});
    });
};

userController.info = function (req, res, next) {
    if (req.query.id == req.user._id)
        return res.redirect('/dashboard/user');
    User.findById(req.query.id, function (err, user) {
        if (err) {
            console.log(err);
            return res.redirect('/dashboard/user');
        }
        res.render('dashboard/userinfo', {title: 'Edit User', username: req.user.username, user: user});
    });
};

userController.insert = function (req, res, next) {
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../../public/uploads/avatar');
    form.parse(req, function (err, fields, files) {
        if(err) {
            res.redirect('/dashboard/user');
            return;
        }

        var stravatar = files.avatar.path.replace(/^.*[\\\/]/, '');

        if (files.avatar.size == 0) {
            fs.unlinkSync(files.avatar.path);
            stravatar = "noavatar";
        }
        if (fields.id == "auto") {
            var user = new User({
                username: fields.username,
                isactived: fields.isactived == "on",
                fullname: fields.fullname,
                address: fields.address,
                zipcode: fields.zipcode,
                country: fields.country,
                region: fields.region,
                phone: fields.phone,
                avatar: stravatar,
                role: fields.role
            });
            User.register(user, "123", function (err, user) {
                if (err)
                    console.log(err);
                res.redirect('/dashboard/user');
            });
        } else {
            User.findById(fields.id, function (err, user) {
                if (err) {
                    res.redirect('/dashboard/user');
                    return;
                }
                user.username= fields.username;
                user.isactived= fields.isactived == "on";
                user.fullname= fields.fullname;
                user.address= fields.address;
                user.zipcode= fields.zipcode;
                user.country= fields.country;
                user.region= fields.region;
                user.phone= fields.phone;
                if (stravatar != "noavatar") {
                    user.avatar= stravatar;
                }
                user.role= fields.role;
                user.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                    res.redirect('/dashboard/user');
                })
            });
        }
    });
};

userController.remove = function (req, res, next) {
    if (req.user._id == req.query.id)
        return res.send('current acc');
    User.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send('OK');
        }
    });
};

userController.new = function (req, res, next) {
    res.render('dashboard/userinfo', {
        title: 'New User', username: req.user.username,
        user: {
            _id: 'auto',
            isactived: false,
            fullname: '',
            address: '',
            zipcode: '',
            country: '',
            region: '',
            phone: ''
        }
    });
};

module.exports = userController;
