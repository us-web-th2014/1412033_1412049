var Comment = require('../../models/comment');
var commentController = {};

commentController.index = function (req, res, next) {
    Comment.find({}, function (err, comments) {
        if(err) return res.send("ERROR");
        res.render('dashboard/comment', {title: "Comment manager", username: req.user.username, comments: comments});
    })
};

commentController.remove = function (req, res, next) {
    Comment.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send('OK');
        }
    });
};

module.exports = commentController;
